//
//  NewsModel.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import Foundation

struct NewsModel : Codable {
    let status : String?
    let feed : Feed?
    let items : [Items]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case feed = "feed"
        case items = "items"
    }
}

struct Items : Codable {
    let title : String?
    let pubDate : String?
    let link : String?
    let guid : String?
    let author : String?
    let thumbnail : String?
    let description : String?
    let content : String?
    let enclosure : Enclosure?
    let categories : [String]?

    enum CodingKeys: String, CodingKey {

        case title = "title"
        case pubDate = "pubDate"
        case link = "link"
        case guid = "guid"
        case author = "author"
        case thumbnail = "thumbnail"
        case description = "description"
        case content = "content"
        case enclosure = "enclosure"
        case categories = "categories"
    }

}

struct Feed : Codable {
    let url : String?
    let title : String?
    let link : String?
    let author : String?
    let description : String?
    let image : String?

    enum CodingKeys: String, CodingKey {

        case url = "url"
        case title = "title"
        case link = "link"
        case author = "author"
        case description = "description"
        case image = "image"
    }
}

struct Enclosure : Codable {
    let link : String?
    let type : String?
    let thumbnail : String?

    enum CodingKeys: String, CodingKey {

        case link = "link"
        case type = "type"
        case thumbnail = "thumbnail"
    }
}
