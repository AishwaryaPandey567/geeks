//
//  WebService.swift
//
//

import Foundation


class WebServiceManager {
    
    static let shared = WebServiceManager()
    
    private var session = URLSession.shared
    
    func getHeaders() -> HTTPHeaders {
        var headers = [HTTPHeaderField.contentType: HTTPHeaderField.applicationJson]
        return headers
    }
    
    func callApi<T : Codable>(urlString: String, requestType : RequestType, requestBody : [String : Any]?, requestParameters : [String : Any]?, completion: @escaping (Result<T, ApiError>) -> ()) {
        
        guard let urlComponent = NSURLComponents(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") else { return }
        
        if let parameters = requestParameters {
            var items = [URLQueryItem]()
            for (key, value) in parameters {
                items.append(URLQueryItem(name: key, value: value as? String ))
            }
            urlComponent.queryItems = items
        }
        
        guard let requestURl = urlComponent.url else {
            return }
        
        var request = URLRequest(url: requestURl)
        
        switch requestType {
        
        case .PUT:
            request.httpMethod = HTTPMethod.PUT
            do {
                let data = try JSONSerialization.data(withJSONObject: requestBody ?? ["": ""], options: [])
                request.httpBody = data
            } catch {
                completion(.failure(.invalidRequest))
                return
            }
            request.allHTTPHeaderFields = self.getHeaders()
            
        case .GET:
            request.httpMethod = HTTPMethod.GET
            
            request.allHTTPHeaderFields = self.getHeaders()
            
        case .POST:
            request.httpMethod = HTTPMethod.POST
            do {
                let data = try JSONSerialization.data(withJSONObject: requestBody ?? ["": ""], options: .prettyPrinted)
                request.httpBody = data
            } catch {
                return
            }
            request.allHTTPHeaderFields = self.getHeaders()
        }
        
        session.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                if let err = error {
                    
                    completion(.failure(.clientError))
                    return
                }
                
                guard let response = response as? HTTPURLResponse else {
                    completion(.failure(.noData))
                    return
                }
                
                if (200...499).contains(response.statusCode) {
                    guard let data = responseData else { return }
                    
                    if let JSONString = data.prettyPrintedJSONString {
                        print(JSONString)
                    }
                    do {
                        let decoder = JSONDecoder()
                        // decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let modelData = try decoder.decode(T.self, from: data)
                        completion(.success(modelData))
                        return
                        
                    } catch {
                        completion(.failure(.JSONError))
                        return
                    }
                } else {
                    
                    guard let data = responseData else { return }
                    
                    if let JSONString = data.prettyPrintedJSONString {
                        print(JSONString)
                    }
                    do {
                        let decoder = JSONDecoder()
                        // decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let modelData = try decoder.decode(T.self, from: data)
                        completion(.success(modelData))
                        return
                        
                    } catch {
                        completion(.failure(.JSONError))
                        return
                    }
                }
            }
            
        }.resume()
    }
}

enum ApiError: Error {
    
    case clientError
    case serverError
    case noData
    case invalidStatusCode
    case JSONError
    case invalidRequest
    case responseError(error: String)
}

enum RequestType {
    case PUT
    case GET
    case POST
}

extension Data {
    var prettyPrintedJSONString: NSString? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }
        return prettyPrintedString
    }
}

public typealias HTTPHeaders = [String: String]

struct HTTPMethod {
    static let GET = "GET"
    static let POST = "POST"
    static let PUT = "PUT"
    static let DELETE = "DELETE"
}
struct HTTPHeaderField {
    static let Accept = "Accept"
    static let oAuthToken = "Authorization"
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
}
