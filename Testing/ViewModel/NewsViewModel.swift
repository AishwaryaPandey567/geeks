//
//  NewsViewModel.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import Foundation

class  NewsViewModel {
    
    func fetchNewsData(completion: @escaping (Result<NewsModel, ApiError>) -> Void) {
        AppDelegate().delegate.showLoaderView()
        let service = WebServiceManager.shared
        let urlString = NetworkAccess.ProductionServer.baseURL
       
        service.callApi(urlString: urlString, requestType: .GET, requestBody: nil, requestParameters: nil) { (result: Result<NewsModel, ApiError>) in
            switch result {
            case .success(let response):
                AppDelegate().delegate.hideLoaderView()
                completion(.success(response))
            case .failure(let err):
                AppDelegate().delegate.hideLoaderView()
                completion(.failure(err))
            }
        }
    }
}
