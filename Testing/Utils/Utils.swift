//
//  Utils.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import Foundation
import UIKit

class Utils{
    func displayAlert(title: String, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
}

