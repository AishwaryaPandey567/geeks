//
//  AppDelegate.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var loaderBgView:UIView?
    var indicatorView:UIActivityIndicatorView?
    var delegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    func showLoaderView() {
        if loaderBgView == nil {
            loaderBgView = UIView(frame: self.window!.frame)
            loaderBgView!.backgroundColor = UIColor.clear
            let loadingView: UIView = UIView()
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = loaderBgView!.center
            loadingView.backgroundColor = .clear//#colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            indicatorView = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0))
           // indicatorView!.style = .whiteLarge
            indicatorView!.color = #colorLiteral(red: 0.8862745098, green: 0.2745098039, blue: 0.5607843137, alpha: 1)
            indicatorView!.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
            loadingView.addSubview(indicatorView!)
            loaderBgView!.addSubview(loadingView)
        }
        indicatorView!.startAnimating()
        self.window!.addSubview(loaderBgView!)
    }
    /**
     Hides loader view in screen
     */
    func hideLoaderView() {
        if loaderBgView != nil {
            indicatorView!.stopAnimating()
            loaderBgView!.removeFromSuperview()
        }
    }


}

