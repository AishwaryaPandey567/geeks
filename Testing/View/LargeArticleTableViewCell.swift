//
//  LargeArticleTableViewCell.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import UIKit

final class LargeArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsContent: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    
    private let imageCache = NSCache<AnyObject, UIImage>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func showNewsData(_ data: Items){
        let dateTime = data.pubDate?.split{$0 == " "}.map(String.init)
        let requiredFormat = dateTime?[0].toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy , h:mm a" )
        dateLabel.text = requiredFormat
        let imageValue = getImageUrl(data.enclosure?.link ?? "")
        let imgUrl = NetworkAccess.ProductionServer.imageBaseUrl + imageValue
        let image = URL(string: (imgUrl))!
        loadImage(fromURL: image, placeHolderImage: "")
        newsContent.text = data.title
    }
    
    func getImageUrl(_ imageUrl: String) -> String{
        let imageBaseUrl = imageUrl.components(separatedBy: "https://live-production.wcms.abc-cdn.net.au/")
        let jje = imageBaseUrl[1].components(separatedBy: "?")
        print(imageBaseUrl,jje)
        return jje[0]
    }
    
   private func loadImage(fromURL imageURL: URL, placeHolderImage: String)
    {
        newsImage.image = UIImage(named: placeHolderImage)

        if let cachedImage = self.imageCache.object(forKey: imageURL as AnyObject)
        {
            newsImage.image = cachedImage
            return
        }

        DispatchQueue.global().async {
            [weak self] in

            if let imageData = try? Data(contentsOf: imageURL)
            {
                if let image = UIImage(data: imageData)
                {
                    DispatchQueue.main.async {
                        self!.imageCache.setObject(image, forKey: imageURL as AnyObject)
                        self?.newsImage.image = image
                    }
                }
            }
        }
    }
}

extension String
{
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
