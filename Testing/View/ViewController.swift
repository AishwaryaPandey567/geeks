//
//  ViewController.swift
//  Testing
//
//  Created by Aishwarya on 18/07/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var largeArticleCell = "LargeArticleTableViewCell"
    private let regularTableCell = "RegularTableViewCell"
    private var viewModel = NewsViewModel()
    private var newsItem = [Items]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
           tableView.refreshControl = refreshControl
        feedsApi()
    }
    
   @objc func doSomething(refreshControl: UIRefreshControl) {
        feedsApi()
        refreshControl.endRefreshing()
    }
    
    func feedsApi() {
        self.viewModel.fetchNewsData { [unowned self] (result: Result<NewsModel, ApiError>) in
            switch result {
            case .success(let response):
                if response.status == "ok"{
                    self.newsItem = response.items ?? []
                    tableView.reloadData()
                }else{
                    Utils().displayAlert(title: "Alert", message: "Something went wrong", viewController: self)
                }
            case .failure(let err):
                Utils().displayAlert(title: "Alert", message: "\(err)", viewController: self)
                break
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: largeArticleCell, bundle: nil), forCellReuseIdentifier: largeArticleCell)
        tableView.register(UINib(nibName: regularTableCell, bundle: nil), forCellReuseIdentifier: regularTableCell)
        if indexPath.row%2 == 0{
        if let cell = tableView.dequeueReusableCell(withIdentifier: largeArticleCell, for: indexPath)as? LargeArticleTableViewCell{
            cell.showNewsData(newsItem[indexPath.row])
            return cell
        }
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: regularTableCell, for: indexPath)as? RegularTableViewCell{
                cell.showNewsData(newsItem[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
